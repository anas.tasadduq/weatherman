"""Imports"""
import sys
import os
import openpyxl as opx


"""Variables Defined"""
num_reports = int((len(sys.argv) - 2) / 2)
report_index = 2
readings = {}


"""Classes Defined"""
class Colors:
    RED = '\033[91m'
    BLUE = '\033[34m'
    RESET = '\033[0m'

class FileAndReadingsHandler:
    def __init__(self, report_time):
        self.report_time = report_time
        self.dir = sys.argv[1]

    def __get_file_paths(self, query):
        files_list = []
        for root, _, files in os.walk(self.dir):
            for file in files:
                if file[0] != '.' and query in file and '~$' not in file:
                    files_list.append(os.path.join(root, file))
        return files_list

    def __get_month(self, file_path):
        month = []
        header = ''
        if file_path.endswith('.xlsx'):
            file = opx.load_workbook(file_path)
            data = []
            for row in file.active.iter_rows():
                data.append([item.value for item in row])
            header = data[0]
            month = data[1:]
            file.close()
        elif file_path.endswith('.tsv'):
            file = open(file_path, 'r')
            lines = file.readlines()
            data = []
            for line in lines:
                data.append([item.strip() for item in line.split('\t')])
            header = data[0]
            month = data[1:]
            file.close()
        else:
            file = open(file_path, 'r')
            lines = file.readlines()
            data = []
            for line in lines:
                data.append([item.strip() for item in line.split(',')])
            header = data[0]
            month = data[1:]
            file.close()
        return header, month

    def get_readings(self, mulitple_files = False):
        if not mulitple_files:
            monthly_readings = {}
            file_months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            year, month = report_time.split('/')
            file_path = self.__get_file_paths(year + '_' + file_months[int(month)])[0]
            header, month_data = self.__get_month(file_path)
            for day_data in month_data:
                daily_readings = {}
                for i in range(len(header)):
                    try:
                        daily_readings[header[i]] = int(day_data[i])
                    except:
                        try:
                            daily_readings[header[i]] = float(day_data[i])
                        except:
                            daily_readings[header[i]] = day_data[i]
                monthly_readings[int(day_data[0].split('-')[2])] = daily_readings
            return file_months[int(month)] + ' ' + year, monthly_readings
        else:
            yearly_readings = {}
            output_months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            file_paths = self.__get_file_paths(self.report_time)
            for file_path in file_paths:
                monthly_readings = {}
                header, month_data = self.__get_month(file_path)
                for day_data in month_data:
                    daily_readings = {}
                    for i in range(len(header)):
                        try:
                            daily_readings[header[i]] = int(day_data[i])
                        except:
                            try:
                                daily_readings[header[i]] = float(day_data[i])
                            except:
                                daily_readings[header[i]] = day_data[i]
                    monthly_readings[int(day_data[0].split('-')[2])] = daily_readings
                yearly_readings[output_months[int(day_data[0].split('-')[1])]] = monthly_readings
            return yearly_readings


"""Main Code"""
for i in range(num_reports):
    report_type = sys.argv[report_index]
    report_time = sys.argv[report_index + 1]
    fh = FileAndReadingsHandler(report_time)

    if num_reports > 1:
        print()
        print('Report:', i + 1)
    
    if report_type == '-e':
        readings = fh.get_readings(mulitple_files = True)
        print(report_time)
        computations = {
            'highest': 0,
            'highest_day': '',
            'highest_month': '',
            'lowest': 0,
            'lowest_day': '',
            'lowest_month': '',
            'humidity': 0,
            'humidity_day': '',
            'humidity_month': '',
        }
        for month, month_data in readings.items():
            for day, day_data in month_data.items():
                temp = day_data['Max TemperatureC']
                if not temp == None and temp > computations['highest']:
                    computations['highest'] = temp
                    computations['highest_day'] = day
                    computations['highest_month'] = month
                temp = day_data['Min TemperatureC']
                if not temp == None and temp < computations['lowest']:
                    computations['lowest'] = temp
                    computations['lowest_day'] = day
                    computations['lowest_month'] = month
                temp = day_data['Max Humidity']
                if not temp == None and temp > computations['humidity']:
                    computations['humidity'] = temp
                    computations['humidity_day'] = day
                    computations['humidity_month'] = month
        print('Highest: ' + str(computations['highest']) + 'C on ' + computations['highest_month'] + ' ' + str(computations['highest_day']))
        print('Lowest: ' + str(computations['lowest']) + 'C on ' + computations['lowest_month'] + ' ' + str(computations['lowest_day']))
        print('Humidity: ' + str(computations['humidity']) + '% on ' + computations['humidity_month'] + ' ' + str(computations['humidity_day']))
    
    else:
        date_string, readings = fh.get_readings()
        print(date_string)

        if report_type == '-a':
            computations = {
                'highest_sum': 0,
                'highest_count': 0,
                'lowest_sum': 0,
                'lowest_count': 0,
                'humidity_sum': 0,
                'humidity_count': 0,
            }
            for day_data in readings.values():
                if not day_data['Max TemperatureC'] == None:
                    computations['highest_sum'] += day_data['Max TemperatureC']
                    computations['highest_count'] += 1
                if not day_data['Min TemperatureC'] == None:
                    computations['lowest_sum'] += day_data['Min TemperatureC']
                    computations['lowest_count'] += 1
                if not day_data['Mean Humidity'] == None:
                    computations['humidity_sum'] += day_data['Mean Humidity']
                    computations['humidity_count'] += 1
            print('Highest Average: ' + str(int(computations['highest_sum'] / computations['highest_count'])) + 'C')
            print('Lowest Average: ' + str(int(computations['lowest_sum'] / computations['lowest_count'])) + 'C')
            print('Average Mean Humidity: ' + str(int(computations['humidity_sum'] / computations['humidity_count'])) + '%')
        
        elif report_type == '-c':
            for day, day_data in readings.items():
                temp = day_data['Max TemperatureC']
                if not temp == None:
                    print('{:0>2d}'.format(day) + ' ' + Colors.RED + '+'*temp + ' ' + str(temp) + 'C' + Colors.RESET)
                temp = day_data['Min TemperatureC']
                if not temp == None:
                    print('{:0>2d}'.format(day) + ' ' + Colors.BLUE + '+'*temp + ' ' + str(temp) + 'C' + Colors.RESET)
        
        elif report_type == '-b':
            for day, day_data in readings.items():
                max = day_data['Max TemperatureC']
                min = day_data['Min TemperatureC']
                if not max == None and not min == None:
                    dif = max - min
                    print('{:0>2d}'.format(day) + ' ' + Colors.BLUE + '+'*min + Colors.RED + '+'*dif + Colors.BLUE + ' ' + str(min) + f'C{Colors.RESET} - {Colors.RED}' + str(max) + f'C{Colors.RESET}')
        else:
            print('Report type selected is not valid')
    
    report_index += 2